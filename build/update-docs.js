var fs = require('fs');

var docs = process.argv.slice(2);

var minified = fs.readFileSync(__dirname + '/../sanitiny.min.js');
var minifiedBytes = minified.length;

console.log('Minified code is ' + minifiedBytes + ' bytes\n');

docs.forEach(function (filename) {
    var text = fs.readFileSync(filename, {encoding: 'utf-8'});
    text = text.replace(/[0-9]+ bytes/g, minifiedBytes + ' bytes');
    fs.writeFileSync(filename, text);
});
