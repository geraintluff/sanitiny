var assert = require('chai').assert;

var api = require('../');

describe('sanity checks', function () {
    var options = {
        tags: ['div', 'a', 'iframe'],
        attributes: ['width', 'height', 'href', 'src', 'frameborder']
    };

    var examples = {
        '<div>test</div>': '<div>test</div>',
        '<a extra="foo">test</a>': '<a>test</a>',
        '<a extra="foo>bar">test</a>': '<a>test</a>',
        '<a href=foo"bar">test</a>': '<a href=foo>test</a>',
        '<a extra=\'foo\'href=\'bar\'>test</a>': '<a href=\'bar\'>test</a>',
        '<a href="javascript:alert(\'pwnd\')">test</a>': '<a href="javascript:void(0)">test</a>',
        '<foo>blah</foo>': 'blah',
        '<foo/>blah': 'blah',
        '<div foo="test"/>bar': '<div/>bar',
        'test<div': 'test<div>',
        'test<a href': 'test<a href>',
        'test<a href=': 'test<a href=>',
        'test<a href="foo': 'test<a href="foo">',
        'test<a href="foo"': 'test<a href="foo">',
        'test<foo ': 'test',
        'test<foo bar="xyz': 'test',
        '<div><script>foo</script>bar</div>': '<div>bar</div>',
        '<div><script>foo</ script >bar</div>': '<div>bar</div>',
        '<div>foo<script>foo bar': '<div>foo',
        'foo<script>bar</script>': 'foo',
        'foo<script>bar<p>test</p></script>': 'foo',
        'foo<style>bar<p>test</p></style>': 'foo',
        'foo<style>bar</style><div>bar</div>': 'foo<div>bar</div>'
    };

    function norm(str) {
        return str.replace(/\s+/g, ' ').replace(/\s+>/g, '>').replace(/\s+\//, '/');
    }

    Object.keys(examples).forEach(function (key) {
        it(key, function () {
            var result = api(key, options);
            assert.deepEqual(norm(result), norm(examples[key]));
        });
    })
});

describe('options', function () {
    var options = {
        tags: [api.defaults.tags, 'iframe', 'foo', 'style']
    };

    var examples = {
        'test<foo>bar</foo': 'test<foo>bar</foo>',
        'foo<style>x < y</style><div>bar</div>': 'foo<style>x < y</style><div>bar</div>'
    };

    function norm(str) {
        return str.replace(/\s+/g, ' ').replace(/\s+>/g, '>').replace(/\s+\//, '/');
    }

    Object.keys(examples).forEach(function (key) {
        it(key, function () {
            var result = api(key, options);
            assert.deepEqual(norm(result), norm(examples[key]));
        });
    })
});
