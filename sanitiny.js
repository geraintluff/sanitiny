(function (global, moduleName, factory) {
    if (typeof module == 'object' && module.exports) {
        module.exports = factory();
    } else if (typeof define == 'function' && define.amd) {
        define(moduleName, factory);
    } else {
        global[moduleName] = factory();
    }
})(this, 'sanitiny', function () {
    // Hacks to reduce code length
    var attributeString = 'attributes';
    var toLowerCase = attributeString.toLowerCase;

    var defaults = {
        tags: 'blockquote,p,a,b,i,strong,em,strike,div',
        schemes: 'http,https,ftp,mailto'
    };
    defaults[attributeString] = 'src,href,name,target';

    function sanitiny(html, options) {
        function getOptionArray(key) {
            return (((options && options[key]) || defaults[key]) + "").split(',');
        }
        var allowedTags = getOptionArray('tags');
        var allowedAttributes = getOptionArray(attributeString);
        var allowedSchemes = getOptionArray('schemes');
        var filters = (options && options.filter) || {
            src: enforceUriScheme,
            href: enforceUriScheme
        };
        var whitespace = /[\s\r\n]/;
        var identifier = /[a-z0-9.-]/i;

        function enforceUriScheme(value, parts) {
            // First, remove everything after some "safe" chars (? /)
            // then look at anything before the first weird character (: \ &)
            parts = value.split(/[?/]/)[0].split(/[:&\\]/);
            if (parts.length > 1 && !arrayContains(allowedSchemes, parts[0])) {
                value = 'javascript:void(0)';
            }
            return value;
        }
        function arrayContains(array, value) {
            return array.indexOf(toLowerCase.call(value)) + 1;
        }

        // Tools for stepping through the input
        html = html + "";
        var index = 0, currentChar = 1;
        function consume(regex, result) {
            // Returns as many currentCharacters as match the expression
            // and moves index forward to first index that doesn't match
            result = '';
            while ((currentChar = html[index]) && regex.test(currentChar)) {
                result += currentChar;
                index++;
            }
            return result;
        }
        function moveToNext() {
            currentChar = html[++index];
        }

        var cleanResult = '';
        while (cleanResult += consume(/[^<]/), currentChar) {
            // currentChar is always == "<"
            var closer = '';
            moveToNext(); // Skip opening "<"
            if (currentChar == '/') {
                closer = currentChar;
                moveToNext();
            }
            var tagName = consume(identifier);
            var tagResult = '<' + closer + tagName + consume(whitespace);
            while (currentChar && currentChar != '>') {
                var attribute = consume(identifier), value, quote = '';
                var lowerAttribute = toLowerCase.call(attribute);
                var attributeResult;
                if (attribute) {
                    attributeResult = attribute + consume(whitespace);
                    if (currentChar == '=') {
                        attributeResult += currentChar + consume(whitespace);
                        moveToNext();
                        if (currentChar == '"') {
                            quote = currentChar;
                            moveToNext(); // Skip opening quote
                            value = consume(/[^"]/);
                            moveToNext(); // Skip ending quote
                        } else if (currentChar == "'") {
                            quote = currentChar;
                            moveToNext(); // Skip opening quote
                            value = consume(/[^']/);
                            moveToNext(); // Skip opening quote
                        } else {
                            value = consume(identifier);
                        }
                        if (filters[lowerAttribute]) {
                            value = filters[lowerAttribute](value);
                        }
                        attributeResult += quote + value.split('<').join('&lt;') + quote;
                    }

                    // If the attribute is allowed, add it to the tag result
                    if (arrayContains(allowedAttributes, attribute)) {
                        tagResult += attributeResult;
                    }
                    tagResult += consume(whitespace);
                } else {
                    // current character cannot be part of an attribute
                    if (currentChar == '/') {
                        moveToNext();
                        if (currentChar == '>') {
                            tagResult += '/';
                            // We'll exit the loop next iteration and append the ">" then
                        }
                    } else {
                        moveToNext(); // ???? - skip it
                    }
                }
            }
            moveToNext(); // Skip ">"
            tagResult += '>';

            var rawTextElements = ['script', 'style'];
            var rawText = 0;
            for (var i = 0; !rawText && rawTextElements[i]; i++) {
                if (!closer && toLowerCase.call(tagName) == rawTextElements[i]) {
                    rawText = html.substring(index).split(
                        new RegExp('</\\s*' + rawTextElements[i] + '[\\s>]', 'i')
                    )[0];
                    index += rawText.length;
                    // If we end up using this tag, we should include the text too
                    tagResult += rawText;
                }
            }
            if (arrayContains(allowedTags, tagName)) {
                cleanResult += tagResult;
            }
        }
        return cleanResult;
    };
    sanitiny.defaults = defaults;
    return sanitiny;
});
